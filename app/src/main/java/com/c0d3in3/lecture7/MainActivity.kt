package com.c0d3in3.lecture7

import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val images = arrayOf(R.mipmap.ic_doggo_1, R.mipmap.ic_doggo_2,R.mipmap.ic_doggo_3, R.mipmap.ic_doggo_4, R.mipmap.ic_doggo_5,
        R.mipmap.ic_doggo_6, R.mipmap.ic_doggo_7, R.mipmap.ic_doggo_8, R.mipmap.ic_doggo_9) // initializing array with image id's

    private lateinit var imageViews : MutableMap<ImageView, Int> // creating variable of imageViews for later initializing

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageViews = mutableMapOf(imageView1 to -1, imageView2 to -1, imageView3 to -1, imageView4 to -1) // initializing map of imageViews
                                                                                                            // with current image ID's
                                                                                                            // format <ImageView, Int>
        init() // initializing method of listeners
    }

    private fun init(){
        imageView1.setOnClickListener(this) // setting imageView1 on click listener
        imageView2.setOnClickListener(this) // setting imageView2 on click listener
        imageView3.setOnClickListener(this) // setting imageView3 on click listener
        imageView4.setOnClickListener(this) // setting imageView4 on click listener
    }

    override fun onClick(v: View?) {
        val randomImage = images.random() // getting random image from images array
        val randomImageView = imageViews.keys.random() // getting random imageView from imageView's map collection keys
        if(!isImageOnScreen(randomImage)){  //  checking if image is already on screen, if not updating random imageView background with random image
            imageViews[randomImageView] = randomImage // setting imageView's value to randomImage's ID
            randomImageView.setBackgroundResource(randomImage)
        }
        else Toast.makeText(this, "Random image is already on screen, try again!", Toast.LENGTH_SHORT).show() // if image is on screen
                                                                                                                            //show toast about it
    }

    private fun isImageOnScreen(imageId: Int) : Boolean{
        for(image in imageViews){
            if(image.value == imageId) // checking if imageView's value is equal of random imageID, if its true it means that image is already on screen
                return true
        }
        return false
    }
}
